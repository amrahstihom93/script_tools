#! /bin/bash
#####################################################################
#####################################################################
#
# to install:
# curl --silent https://gitlab.com/amrahstihom93/SiPiOS/raw/master/install_script_tools.sh | bash
#
#####################################################################
#####################################################################

PIHOME=/home/pi
SGMW=Sgmw
LIB=lib
SCRIPT=script_tools

pushd $PIHOME > /dev/null
result=${PWD##*/} 
# check if ~/Sgmw exists, if not create it
if [ ! -d $SGMW ] ; then
    mkdir $SGMW
fi
# go into $SGMW
cd $SGMW


# check if /home/pi/Sgmw/lib exists, if not create it
if [ ! -d $LIB ] ; then
    mkdir $LIB
fi
cd $LIB

# check if /home/pi/Sgmw/lib/Dexter exists, if not create it
if [ ! -d $SGMW ] ; then
    mkdir $SGMW
fi
cd $SGMW


# check if /home/pi/Sgmw/lib/script_tools exists
# if yes refresh the folder
# if not, clone the folder
if [ ! -d $SCRIPT ] ; then
    # clone the folder
    sudo git clone --quiet https://gitlab.com/amrahstihom93/script_tools.git
else
    cd $SCRIPT
    sudo git pull --quiet
fi

popd > /dev/null