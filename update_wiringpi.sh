#!/bin/bash
source /home/pi/Sgmw/lib/Dexter/script_tools/functions_library.sh

PIHOME=/home/pi
SGMW=Sgmw
LIB=lib


# Check if WiringPi Installed and has the latest version.  If it does, skip the step.
# Gets the version of wiringPi installed
version=`gpio -v`

# Parses the version to get the number
set -- $version

# Gets the third word parsed out of the first line of gpio -v returned.
# Should be 2.36
WIRINGVERSIONDEC=$3

# Store to temp file
echo $WIRINGVERSIONDEC >> tmpversion

# Remove decimals
VERSION=$(sed 's/\.//g' tmpversion)

# Remove the temp file
delete_file tmpversion

feedback "wiringPi VERSION is $VERSION"
if [ $VERSION -eq '236' ]; then

    feedback "FOUND WiringPi Version 2.36 No installation needed."
else
    feedback "Did NOT find WiringPi Version 2.36"
    # Check if the Sgmw directory exists.
    create_folder "$PIHOME/$SGMW"
    create_folder "$PIHOME/$SGMW/$LIB"

    # Change directories to Sgmw/lib
    cd $PIHOME/$SGMW/$LIB

    delete_folder wiringPi
    # Install wiringPi
    git clone https://gitlab.com/amrahstihom93/wiringPi/  # Clone directories to Dexter.
    cd wiringPi
    sudo chmod +x ./build
    sudo ./build
    feedback "wiringPi Installed"
fi
# End check if WiringPi installed
